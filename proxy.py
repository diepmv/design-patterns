''' Remote,

Virtual: +control access to resource that is expensive to create
+Postpone object creatation unless neccessary

Protection Proxy: Control access resource based on access rights
'''

import time

class Producer:
    def produce(self):
        print("Producer is working hard")
    def meet(self):
        print("Producer has time to meet you now")

class Proxy():
    def __init__(self):
        self.occupied = 'No'
        self.producer = None

    def producer(self):
        """Check if Producer is availabel"""
        print("Artist checking if Producer is available")

        if self.occupied == 'No':
            # If the producer is available, create a producer object
                self.producer = Producer()
                time.sleep(2)
            # take the producer meet the guest
                self.producer.mee()
        else:
            # Otherwise, don't instantiate a producer
            time.sleep(2)
            print("Producer is busy")

# Instantiate a Proxy

p = Proxy() 

# take the proxy: Artist produce until Producer is available 
p.produce()

# change the status to "occupied"

p.occupied = 'Yes'

# Make the Producer produce

p.produce()





####################################

from __future__ import print_function
import time


class SalesManager:
    def talk(self):
        print("Sales Manager ready to talk")


class Proxy:
    def __init__(self):
        self.busy = 'No'
        self.sales = None

    def talk(self):
        print("Proxy checking for Sales Manager availability")
        if self.busy == 'No':
            self.sales = SalesManager()
            time.sleep(0.1)
            self.sales.talk()
        else:
            time.sleep(0.1)
            print("Sales Manager is busy")


class NoTalkProxy(Proxy):
    def talk(self):
        print("Proxy checking for Sales Manager availability")
        time.sleep(0.1)
        print("This Sales Manager will not talk to you", "whether he/she is busy or not")


if __name__ == '__main__':
    p = Proxy()
    p.talk()
    p.busy = 'Yes'
    p.talk()
    p = NoTalkProxy()
    p.talk()
    p.busy = 'Yes'
    p.talk()

### OUTPUT ###
# Proxy checking for Sales Manager availability
# Sales Manager ready to talk
# Proxy checking for Sales Manager availability
# Sales Manager is busy
# Proxy checking for Sales Manager availability
# This Sales Manager will not talk to you whether he/she is busy or not
# Proxy checking for Sales Manager availability
# This Sales Manager will not talk to you whether he/she is busy or not
